import React, { useEffect, useState } from 'react';
import { getPosts } from '../../api/posts';
import { PostModel } from '../../../server/models/PostModel';
import map from 'lodash/map';
import { useHistory } from 'react-router-dom';
import { PostItem } from '../../components/PostItem/PostItem';

import './PostsPage.css';

function usePosts() {

  const [posts, setPosts] = useState([] as Array<Partial<PostModel>>);

  useEffect(() => {
    getPosts().then(setPosts);
  }, []);

  return posts;
}

export const PostsPage = () => {
  const posts = usePosts();

  const history = useHistory();

  const handleItemClick = (slug?: string) => {
    history.push(`/post/${slug}`);
  };

  return (
    <div className="row row-cols-1 row-cols-md-2 g-4">
      {map(posts, (post) => (
        <div className="col post-card" key={post._id}>
          <PostItem item={post} onClick={handleItemClick} />
        </div>
      ))}
    </div>
  )
};
