import { Module } from '@nestjs/common';
import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';
import { DbProvider } from './services/db.provider';
import { DbServer } from './services/db.server';
import { PostService } from './services/post.service';
import { PostsController } from './controllers/posts.controller';

@Module({
  imports: [],
  controllers: [PostsController, UserController],
  providers: [UserService, DbProvider, DbServer, PostService],
  exports: [DbProvider]
})
export class AppModule {}
