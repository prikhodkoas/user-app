import { Controller, Get, Param, Query } from '@nestjs/common';
import { PostService } from '../services/post.service';
import { ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { PostListFilter } from '../models/PostListFilter';
import { PostModel } from '../models/PostModel';

@ApiTags('Post')
@Controller('post')
export class PostsController {

  constructor(
    private postService: PostService
  ) { }

  @ApiQuery({ name: 'authorId', type: String, required: false })
  @ApiQuery({ name: 'slug', type: String, required: false })
  @Get()
  public getAll(@Query() filter: Partial<PostListFilter>): Promise<Array<Partial<PostModel>>> {
    return this.postService.getPosts(filter);
  }

  @ApiParam({ name: 'id', type: String, required: true })
  @Get(':id')
  public getById(@Param('id') id: string) {
    return this.postService.getPostById(id);
  }

}
