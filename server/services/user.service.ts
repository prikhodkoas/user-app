import { Injectable } from '@nestjs/common';
import { DbProvider } from './db.provider';
import { Collection, ObjectId } from 'mongodb';
import { UserModel } from '../models/UserModel';
import { omit } from 'lodash';

@Injectable()
export class UserService {

  private get users(): Collection<UserModel> {
    return this.dbProvider.users;
  }

  constructor(private dbProvider: DbProvider) { }

  public getUser(id: string): Promise<UserModel | null> {
    return this.users.findOne(new ObjectId(id));
  }

  public getUserList(): Promise<Array<UserModel>> {
    return this.users.find(null, {
      projection: {
        _id: true,
        firstName: true,
        lastName: true,
        email: true
      }
    }).toArray();
  }

  public async updateUser(id: string, user: UserModel) {
    const model = omit(user, ['_id']);
    await this.users.updateOne({ _id: new ObjectId(id) }, { $set: model })
  }

  public async createUser(user: UserModel) {
    const model = omit(user, ['_id']);
    const { insertedId } = await this.users.insertOne(model);
    return String(insertedId);
  }

}
