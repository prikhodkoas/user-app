const faker = require('faker');
const range = require('lodash/range');

const createUser = () => {
  return {
    firstName: faker.name.firstName(),

    lastName: faker.name.lastName(),

    email: faker.internet.email(),

    bio: faker.lorem.paragraph(),

    phone: faker.phone.phoneNumber(),

    isVerified: faker.random.boolean(),

    birthday: faker.date.between(new Date('1990-01-01'), new Date('2000-01-01')),
  };
};

module.exports = {
  async up(db, client) {
    const users = range(25).map(createUser);
    await db.collection('users').insertMany(users);
    console.log(`Seeding: insert ${users.length} items to user collection`);
  },

  async down(db, client) {
    // TODO write the statements to rollback your migration (if possible)
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: false}});
  }
};
